FROM php:7.3-apache

# Copy files, except for composer files
COPY /backend/composer.json /var/www/html/composer.json
COPY /backend/src /var/www/html/src
COPY /backend/config.php /var/www/html/config.php
COPY /backend/index.php /var/www/html/index.php

# Download composer 
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
# Env variable for autoload pat, even though not necessary
ENV ROOT_DIR=/var/www/html/vendor/autoload.php
WORKDIR /var/www/html

# Install composer dependencies
RUN composer install