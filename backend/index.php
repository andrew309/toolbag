<?php
    declare(strict_types=1);

    // Import config file to pass getRootDir() for autoloader
    require_once 'config.php';
    require_once ToolBag\Config::getRootDir();
    
    $home = new ToolBag\Home();
    $home->get();
?>